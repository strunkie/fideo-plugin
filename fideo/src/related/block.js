/**
 * BLOCK: fideo
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InnerBlocks, InspectorControls, } = wp.blockEditor;
const { RangeControl, PanelBody, BaseControl , SelectControl, Tooltip, TextControl, ToggleControl } = wp.components;
const { Component, Fragment } = wp.element;

class Related extends Component {
	constructor() {
		super( ...arguments );

		this.state = {
			loadedItems: [],
			selectedItems: [],
		}
	}

	componentDidMount() {
		this.fetchItems();
	}

	fetchItems() {
		const { attributes, setAttributes } = this.props;

		fetch(`/wp-json/fideo/v1/pages_by_type?type=${attributes.contenttype}`)
			.then(response => response.json())
			.then(data => {
				this.setState({ loadedItems: data });
			});
	}

	createItems() {
		const { attributes, setAttributes } = this.props;

		const formattedLoadedItems = this.state.loadedItems.map(item => {
	    	return { label: item.title, value: item.id };
	    });

	    const options = [{
	    	label: 'Willekeurig item',
	    	value: -1
	    }, ...formattedLoadedItems];

		const items = [];
		for (let i = 0; i < attributes.nrofitems; i++) {
			items.push(
				<div class="overview__items__item">
					<SelectControl
			    	    label='Selecteer item'
			    	    value={ attributes.selectedItems[i] }
			    	    options={ options }
			    	    onChange={ (value) => {
			    	    	const items = [...attributes.selectedItems];
			    	    	items[i] = value;
			    	    	setAttributes( { selectedItems: items } );
			    	    } }
			    	/>
				</div>
			);
		}

		return items;
	}

	render() {
		const { attributes, setAttributes, isSelected, clientId, className } = this.props;

		return (
			<Fragment>
                <InspectorControls>
				    <PanelBody title={ __( 'Instellingen' ) }>
				        <SelectControl
				            label={ __( 'Item categorie' ) }
				            value={ attributes.contenttype }
				            options={ [
				                { label: __( 'Cases' ), value: 'cases' },
				                { label: __( 'Insights' ), value: 'insights' },
				            ] }
				            onChange={ (value) => {
				            	setAttributes( { contenttype: value, selectedItems: [] } );
				            	setTimeout(() => {
				            		this.fetchItems();
				            	})
				            }}
				        />
				        <SelectControl
				            label={ __( 'Aantal items' ) }
				            value={ attributes.nrofitems }
				            options={ [
				                { label: __( '2' ), value: '2' },
				                { label: __( '3' ), value: '3' },
				            ] }
				            onChange={ (value) => setAttributes( { nrofitems: value } ) }
				        />
			        </PanelBody>
				    <PanelBody title={ __( 'CTA button' ) }>
				    	<SelectControl
				    	    label={ __( 'Knop locatie' ) }
				    	    value={ attributes.buttonstyle }
				    	    options={ [
				    	        { label: __( 'Geen button' ), value: '1' },
				    	        { label: __( 'Naast title' ), value: '2' },
				    	        { label: __( 'Onder items' ), value: '3' },
				    	    ] }
				    	    onChange={ (value) => setAttributes( { buttonstyle: value } ) }
				    	/>
				    	<TextControl
				        	label="Knop tekst"
				        	value={ attributes.buttontext }
				        	onChange={ (value) => setAttributes( { buttontext: value } ) }
				        />
				        <TextControl
				        	label="Knop link"
				        	value={ attributes.buttonlink }
				        	onChange={ (value) => setAttributes( { buttonlink: value } ) }
				        />
			        </PanelBody>
				</InspectorControls>

				<div className={ className }>
                    <RichText value={attributes.title} onChange={(value) => { setAttributes({ title: value }) }} />

                    <div class="overview-items">
                    	{this.createItems()}
                    </div>
				</div>
			</Fragment>
		);
	}

}

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/related', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'related' ), // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [],
	attributes: {
		title: {
			type: 'string',
			default: ''
		},
		buttonstyle: {
			type: 'string',
			default: '1'
		},
		buttontext: {
			type: 'string',
			default: ''
		},
		buttonlink: {
			type: 'string',
			default: ''
		},
		contenttype: {
			type: 'string',
			default: 'cases'
		},
		nrofitems: {
			type: 'string',
			default: '2'
		},
		selectedItems: {
			type: 'array',
			default: []
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: Related,

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		return null;
	},
} );
