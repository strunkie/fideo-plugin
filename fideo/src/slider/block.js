import './style.scss';
import './editor.scss';
import 'slick-carousel';

window.$ = jQuery;

(function ( wpI18n, wpBlocks, wpElement, wpEditor, wpComponents ) {
    const { __ } = wpI18n;
    const { Component, Fragment } = wpElement;
    const { registerBlockType } = wpBlocks;
    const { InspectorControls, PanelColorSettings, MediaUpload } = wpEditor;
    const { PanelBody, RangeControl, ToggleControl , SelectControl, TextControl, TextareaControl, IconButton, Button, Placeholder, Tooltip } = wpComponents;

    class ImageGallery extends Component {
        constructor() {
            super( ...arguments );
            this.state = {
                currentSelected: 0,
                inited: false,
            };

            this.initSlider = this.initSlider.bind(this);
        }

        componentDidMount() {
            const { attributes } = this.props;

            if (attributes.images.length) {
                this.initSlider();
            }
        }

        componentWillUpdate( nextProps ) {
            const { clientId, attributes } = this.props;
            const { images } = attributes;
            const { images: nextImages } = nextProps.attributes;

            if ( images.length !== nextImages.length ) {
                $(`#block-${clientId} .slider__slides.slick-initialized`).slick('unslick');
                $(`#block-${clientId} .image-slider-item`)
                    .removeAttr('tabindex')
                    .removeAttr('role')
                    .removeAttr('aria-describedby');
            }
        }

        componentDidUpdate( prevProps ) {
            const { attributes, isSelected } = this.props;
            const { images } = attributes;
            const { images: prevImages } = prevProps.attributes;

            if (images.length !== prevImages.length) {
                if (images.length) {
                    setTimeout(() => this.initSlider(), 100);
                } else if (images.length === 0 && this.state.inited) {
                    this.setState( { inited: false } );
                }
            }

            if (!this.state.inited && isSelected) {
                this.setState( { inited: true } );
            }

            if (!isSelected && this.state.inited) {
                this.setState( { inited: false } );
            }
        }

        initSlider() {
            const { clientId } = this.props;

            $(`#block-${clientId} .slider__slides:not(.slick-initialized)`).slick( {
                dots: false,
                arrows: false,
            } );

            $(`#block-${clientId} .slider__slides`).on('afterChange', (e, s, currentSlide) => {
                if (this.state.currentSelected !== currentSlide) {
                    this.setState( { currentSelected: currentSlide } );
                }
            } );

            $(`#block-${clientId}`).on('click', '.slider__button--prev', () => {
            	$(`#block-${clientId} .slider__slides`).slick('slickPrev');
            })

            $(`#block-${clientId}`).on('click', '.slider__button--next', () => {
            	$(`#block-${clientId} .slider__slides`).slick('slickNext');
            })
        }

        updateImagesData(data) {
            const { currentSelected } = this.state;
            if (typeof currentSelected !== 'number') {
                return null;
            }

            const { attributes, setAttributes } = this.props;
            const { images } = attributes;

            const newImages = images.map( (image, index) => {
                if (index === currentSelected) {
                    image = { ...image, ...data };
                }

                return image;
            } );

            setAttributes( { images: newImages } );
        }

        render() {
            const { attributes, setAttributes, isSelected, clientId, className } = this.props;
            const { currentSelected } = this.state;
            const {
                images,
                actionOnClick,
                width,
                height
            } = attributes;

            if (images.length === 0) {
                return (
                    <Placeholder
                        label={ __( 'Image Slider Block' ) }
                        instructions={ __( 'No images selected. Adding images to start using this block.' ) }
                    >
                        <MediaUpload
                            allowedTypes={ ['image'] }
                            value={ null }
                            multiple
                            onSelect={ (image) => {
                                const imgInsert = image.map( (img) => {
                                    if(!img.sizes.slider || !img.sizes.slider.url) {
                                        alert('afbeelding is te klein en kan niet geselecteerd worden!');
                                    } else {
                                        return {
                                            url: img.sizes.slider.url,
                                            id: img.id,
                                        }
                                    }                     
                                });

                                setAttributes( {
                                    images: [
                                        ...images,
                                        ...imgInsert,
                                    ]
                                } )
                            } }
                            render={ ( { open } ) => (
                                <Button className="button button-large button-primary" onClick={ open }>
                                    { __( 'Add images' ) }
                                </Button>
                            ) }
                        />
                    </Placeholder>
                )
            }

            return (
                <Fragment>
                    <div className={className}>
	                    <div class="slider">
	            	        <div class="slider__slides">
	            	        	{images.map( (image, index) => (
	            	        	    <div class="slider__slide">
                                        <img src={image.url} />
	            	        	        <div class="slider__caption">
	            	        	        	{ image.text }
	            	        	        </div>
	            	        	    </div>
	            	        	) ) }
	            	        </div>
	            	    
	            	        <div class="slider__buttons">
	            	            <div class="slider__button slider__button--prev"><i class="icon-arrow-back"></i></div>
	            	            <div class="slider__button slider__button--next"><i class="icon-arrow-forward"></i></div>
	            	        </div>
            	     	</div>

                        {isSelected && (
                        <div className="image-slider-controls">

                        	<TextareaControl
                        	    label={ __( 'Afbeelding omschrijving' ) }
                        	    value={ images[currentSelected] ? images[currentSelected].text || '' : '' }
                        	    onChange={ (value) => this.updateImagesData( { text: value || '' } ) }
                        	/>

                            <div className="image-slider-image-list">
                                {images.map( (image, index) => (
                                    <div className="image-slider-image-list-item" key={index}>
                                        <img src={ image.url }
                                             className="image-slider-image-list-img"
                                             onClick={ () => {
                                                 $(`#block-${clientId} .slider__slides`).slick('slickGoTo', index, false);
                                                 this.setState( { currentSelected: index } )
                                             } }
                                        />
                                        <Tooltip text={ __( 'Remove image' ) }>
                                            <IconButton
                                                className="image-slider-image-list-item-remove"
                                                icon="no"
                                                onClick={ () => {
                                                    if (index === currentSelected) this.setState( { currentSelected: null } );
                                                    setAttributes( { images: images.filter( (img, idx) => idx !== index ) } )
                                                } }
                                            />
                                        </Tooltip>
                                    </div>
                                ) ) }
                                <div className="image-slider-add-item">
                                    <MediaUpload
                                        allowedTypes={ ['image'] }
                                        value={ currentSelected }
                                        onSelect={ (image) => {
                                            if(!image.sizes.slider || !image.sizes.slider.url) {
                                                alert('afbeelding is te klein en kan niet geselecteerd worden!');
                                            } else {
                                                setAttributes( {
                                                    images: [...images, { id: image.id, url: image.sizes.slider.url, } ],
                                                } )
                                            }           
                                        } }
                                        render={ ( { open } ) => (
                                            <Button
                                                label={ __( 'Add image' ) }
                                                icon="plus"
                                                onClick={ open }
                                            />
                                        ) }
                                    />
                                </div>
                            </div>
                        </div>
                        ) }
                    </div>
                </Fragment>
            )
        }
    }

    const blockAttrs = {
        images: {
            type: 'array',
            default: [], // [ { id: int, url, title, text, link: string } ]
        },
        actionOnClick: {
            type: 'string',
        },
        width: {
            type: 'number',
            default: 700,
        },
        height: {
            type: 'number',
            default: 500,
        },
        changed: {
            type: 'boolean',
            default: false,
        }
    };

    registerBlockType( 'cgb/slider', {
        title: __( 'Afbeeldingen' ),
        description: __( 'Afbeeldingen in een slider' ),
        icon: 'shield',
        category: 'common',
        keywords: [ __( 'slide' ), __( 'gallery' ), __( 'photos' ) ],
        attributes: blockAttrs,
        edit: ImageGallery,
        save: function ( { attributes } ) {
            return null;
        }
    } );
})( wp.i18n, wp.blocks, wp.element, wp.blockEditor, wp.components );