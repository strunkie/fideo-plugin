/**
 * BLOCK: fideo
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { RichText, InnerBlocks, InspectorControls, MediaUpload } = wp.blockEditor;
const { RangeControl, PanelBody, BaseControl , SelectControl, Tooltip, TextControl, ToggleControl } = wp.components;
const { Component, Fragment } = wp.element;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/widget-question', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: 'Vragen Widget', // Block title.
	icon: 'shield', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [],
	attributes: {
		text: {
			type: 'string',
			default: null
		},
		title: {
			type: 'string',
			default: null
		},
		btntext: {
			type: 'string',
			default: null
		},
		btnlink: {
			type: 'string',
			default: null
		}
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( {setAttributes, attributes, className} ) {
		return (
			<Fragment>
                <InspectorControls>
                	<PanelBody title='Knop'>
				        <TextControl
				        	label="Knop link"
				        	value={ attributes.btnlink }
				        	onChange={ (value) => setAttributes( { btnlink: value } ) }
				        />
			        </PanelBody>
				</InspectorControls>                
				<div className={ className }>
					<div class="fideo-field">
						<div class="fideo-field__label">Titel: </div>
						<div class="fideo-field__field"><RichText value={attributes.title} onChange={(value) => {setAttributes({title: value})}} placeholder="titel" /></div>
					</div>

					<div class="fideo-field">
						<div class="fideo-field__label">Tekst: </div>
						
						<div class="fideo-field__field"><RichText value={attributes.text} onChange={(value) => {setAttributes({text: value})}} placeholder="Tekst" /></div>
					</div>

					<div class="fideo-field">
						<div class="fideo-field__label">Knop tekst: </div>
						
						<div class="fideo-field__field"><RichText value={attributes.btntext} onChange={(value) => {setAttributes({btntext: value})}} placeholder="Knop tekst" /></div>
					</div>
				</div>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( ) {
		return null;
	},
} );
