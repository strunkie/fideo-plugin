/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './intro/block.js';
import './button/block.js';
import './page-intro/block.js';
import './slider/block.js';
import './text/block.js';
import './column-sidebar/block.js';
import './column-text/block.js';
import './blockquote/block.js';
import './related/block.js';
import './overview/block.js';
import './usps/block.js';
import './linklist/block.js';
import './logos/block.js';
import './share/block.js';
import './contact-cta/block.js';
import './employees/block.js';
import './jobapply/block.js';
import './vimeo/block.js';
import './youtube/block.js';
import './circle/block.js';
import './steps/block.js';

import './widgetvideo/block.js';
import './widgetquestion/block.js';
